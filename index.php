<?php

require __DIR__ . '/vendor/autoload.php';

use Handlebars\Handlebars;

$view_options = [
    'loader' => new \Handlebars\Loader\FilesystemLoader(__DIR__.'/templates/',['extension'=>'.tmpl']),
];

$engine = new Handlebars($view_options);

$viewVars = [];

$viewVars['Name']  = "Rodrigo G Tschope";
$viewVars['Stuff'] = [
    [
        'Thing' => "roses",
        'Desc'  => "red"
    ],
    [
        'Thing' => "violets",
        'Desc'  => "blue"
    ],
    [
        'Thing' => "you",
        'Desc'  => "able to solve this"
    ],
    [
        'Thing' => "we",
        'Desc'  => "interested in you"
    ]
];

$tmpl = 'extra';

if(isset($_GET['tmpl'])){
    if($_GET['tmpl'] == 'easy')
        $tmpl = 'template';
}

echo $engine->render($tmpl, $viewVars);