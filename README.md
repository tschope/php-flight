# The Test

Task:

Build a simple template engine in a language of your choice (extra credit for PHP),
that takes template.tmpl (no touching) and the following variables (format them for your chosen language) as input.

Variables:
```
Name  = "Your name goes here"
Stuff = [
  [
    Thing = "roses",
    Desc  = "red"
  ],
  [
    Thing = "violets",
    Desc  = "blue"
  ],
  [
    Thing = "you",
    Desc  = "able to solve this"
  ],
  [
    Thing = "we",
    Desc  = "interested in you"
  ]
]
```

More extra credit:

Use (and handle) extra.tmpl instead of template.tmpl

# Solution

Hello, my name is Rodrigo and I'm Fullstack PHP Developer, with more experience in PHP but worked with some web languages like Javascript, Python, Java, HTML, CSS and etc..

Before you run index.php

- You don't need to run composer install because I committed folder vendor to give you less work, just for this test, I know, we shouldn't commit vendor folder.
- I put your templates in folder templates only to organize the project.

If your are in Linux or macOS just run in project folder:
```
php -S localhost:9000
```

To see file template.tmpl use this GET variable:
```
?tmpl=easy
```

I made simple way, only with simple index, without any framework or REST api or all of this new techiniques they use at this moment. If you want I can do this project in some framework, create a REST API, use React, AngularJS, models and all this stuff. It's up to you.

[Extra Template](http://localhost:9000/) and [Template](http://localhost:9000/?tmpl=easy)

## Rodrigo Tschope
### PHP Developer - Fullstack Developer - DevOps
- [LinkedIn](https://www.linkedin.com/in/tschope/)
- [WebSite](https://tschope.tech)
- Skype: rodrigo.tschope@hotmail.com / tschope
- GTalk: tschope@gmail.com

#### Cheers



